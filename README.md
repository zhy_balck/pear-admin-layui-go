# Pear Admin Layui Go

#### 介绍
基于gin gorm的快速后台开发模板

#### 软件架构
go gin gorm

#### 安装教程

1.  sql/pear-admin-layui-go.sql 初始化数据库
2.  修改数据库链接 framework/database/database.go
3.  启动项目 main.go

#### 使用说明
![输入图片说明](images/QQ%E6%88%AA%E5%9B%BE20230129092832.png)
![输入图片说明](images/QQ%E6%88%AA%E5%9B%BE20230129092845.png)
![输入图片说明](images/QQ%E6%88%AA%E5%9B%BE20230129092852.png)
![输入图片说明](images/QQ%E6%88%AA%E5%9B%BE20230129092902.png)
![输入图片说明](images/QQ%E6%88%AA%E5%9B%BE20230129092907.png)
![输入图片说明](images/QQ%E6%88%AA%E5%9B%BE20230129092914.png)
![输入图片说明](images/QQ%E6%88%AA%E5%9B%BE20230129092919.png)
![输入图片说明](images/QQ%E6%88%AA%E5%9B%BE20230129092926.png)
![输入图片说明](images/QQ%E6%88%AA%E5%9B%BE20230129092931.png)

#### 联系方式

邮箱：www.qq1624.com@qq.com
