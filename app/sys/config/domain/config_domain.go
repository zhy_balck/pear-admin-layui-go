package config_domain

import ft "pear-admin-layui-go/framework/table"

type ConfigEnableRequest struct {
	Id         int64 `form:"id"`
	Enable     bool  `form:"enable"`
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

type Config struct {
	Id         int64
	Name       string
	Value      string
	Enable     bool
	Type       int
	Status     int
	Remark     string
	DelFlag    bool
	CreateTime int64
	CreateBy   int64
	CreateName string
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

func (Config) TableName() string {
	return "sys_config"
}

type ConfigEditRequest struct {
	Id         int64  `form:"id"`
	Name       string `form:"name"`
	Value      string `form:"value"`
	Remark     string `form:"remark"`
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

type ConfigSearchRequest struct {
	ft.TableRequest
	Name string `form:"name"`
}

type ConfigSaveRequest struct {
	Name       string `form:"name"`
	Value      string `form:"value"`
	Enable     bool   `form:"enable"`
	Remark     string `form:"remark"`
	CreateTime int64
	CreateBy   int64
	CreateName string
}

type ConfigTableResponse struct {
	Id         int64  `json:"id"`
	Name       string `json:"name"`
	Value      string `json:"value"`
	Enable     bool   `json:"enable"`
	Remark     string `json:"remark"`
	CreateTime int64  `json:"createTime"`
	CreateName string `json:"createName"`
	UpdateTime int64  `json:"updateTime"`
	UpdateName string `json:"updateName"`
}
