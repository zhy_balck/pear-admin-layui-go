package log_controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	ld "pear-admin-layui-go/app/sys/log/domain"
	ls "pear-admin-layui-go/app/sys/log/service"
	ft "pear-admin-layui-go/framework/table"
)

func Table(c *gin.Context) {
	var ltr ld.LogTableRequest
	_ = c.ShouldBind(&ltr)
	table, count := ls.Table(&ltr)
	var ltrs = make([]ld.LogTableResponse, len(table))
	for i, v := range table {
		ltrs[i] = ld.LogTableResponse{
			Id:         v.Id,
			Ipaddr:     v.Ipaddr,
			Url:        v.Url,
			Method:     v.Method,
			Param:      v.Param,
			Result:     v.Result,
			CreateTime: v.CreateTime,
			CreateBy:   v.CreateBy,
			CreateName: v.CreateName,
		}
	}
	c.JSON(http.StatusOK, ft.TableResponse{
		Count: count,
		Data:  ltrs,
	})
}

func LogPage(c *gin.Context) {
	c.HTML(http.StatusOK, "sys/log/log.html", gin.H{})
}
