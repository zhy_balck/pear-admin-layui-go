package log_domain

import ft "pear-admin-layui-go/framework/table"

type LogDb struct {
	Id         int64
	Host       string
	Ipaddr     string
	Url        string
	Method     string
	Param      string
	Result     string
	StartTime  int64
	EndTime    int64
	SpendTime  int64
	UserAgent  string
	Type       int
	Status     int
	Remark     string
	DelFlag    bool
	CreateTime int64
	CreateBy   int64
	CreateName string
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

func (LogDb) TableName() string {
	return "sys_log"
}

type LogTableRequest struct {
	ft.TableRequest
	Username string `form:"username"`
}

type LogTableResponse struct {
	Id         int64  `json:"id"`
	Ipaddr     string `json:"ipaddr"`
	Url        string `json:"url"`
	Method     string `json:"method"`
	Param      string `json:"param"`
	Result     string `json:"result"`
	CreateTime int64  `json:"createTime"`
	CreateBy   int64  `json:"createBy"`
	CreateName string `json:"createName"`
}
