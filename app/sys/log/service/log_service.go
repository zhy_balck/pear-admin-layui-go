package log_service

import (
	ld "pear-admin-layui-go/app/sys/log/domain"
	fd "pear-admin-layui-go/framework/database"
)

func WebLog(db *ld.LogDb) {
	fd.Db.Save(&db)
}

func Table(ltr *ld.LogTableRequest) ([]ld.LogDb, int64) {
	var count int64
	db := fd.Db.Model(ld.LogDb{}).Where("del_flag = false")
	if ltr.Username != "" {
		db.Where("create_name = ?", ltr.Username)
	}
	db.Count(&count)
	var logs []ld.LogDb
	db.Select("id", "ipaddr", "url", "method", "param", "result", "create_time", "create_by",
		"create_name").Limit(ltr.Limit).Offset((ltr.Page - 1) * ltr.Limit).Find(&logs)
	return logs, count
}
