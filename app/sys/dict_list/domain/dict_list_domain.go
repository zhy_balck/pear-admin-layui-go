package dict_list_domain

import ft "pear-admin-layui-go/framework/table"

type DictList struct {
	Id         int64
	TypeCode   string
	DataLabel  string
	DataValue  string
	IsDefault  bool
	Enable     bool
	DelFlag    bool
	Remark     string
	CreateTime int64
	CreateBy   int64
	CreateName string
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

func (DictList) TableName() string {
	return "sys_dict_list"
}

type DictListSearchTable struct {
	ft.TableRequest
	TypeCode string `form:"typeCode"`
}

type DictListTableResponse struct {
	Id         int64  `json:"id"`
	TypeCode   string `json:"typeCode"`
	DataLabel  string `json:"dataLabel"`
	DataValue  string `json:"dataValue"`
	IsDefault  bool   `json:"isDefault"`
	Enable     bool   `json:"enable"`
	Remark     string `json:"remark"`
	CreateTime int64  `json:"createTime"`
	CreateName string `json:"createName"`
	UpdateTime int64  `json:"updateTime"`
	UpdateName string `json:"updateName"`
}

type DictListEnableRequest struct {
	Id         int64 `form:"id"`
	Enable     bool  `form:"enable"`
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

type DictIsDefaultEnableRequest struct {
	Id         int64 `form:"id"`
	IsDefault  bool  `form:"isDefault"`
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

type DictListSaveRequest struct {
	TypeCode  string `form:"typeCode"`
	DataLabel string `form:"dataLabel"`
	DataValue string `form:"dataValue"`
	IsDefault bool   `form:"isDefault"`
	Enable    bool   `form:"enable"`
	Remark    string `form:"remark"`
	// -----------
	CreateTime int64
	CreateBy   int64
	CreateName string
}

type DictListEditRequest struct {
	Id        int64  `form:"id"`
	DataLabel string `form:"dataLabel"`
	DataValue string `form:"dataValue"`
	Remark    string `form:"remark"`
	// -------------
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

type DictListCodeResponse struct {
	DataLabel string `json:"dataLabel"`
	DataValue string `json:"dataValue"`
	IsDefault bool   `json:"isDefault"`
}
