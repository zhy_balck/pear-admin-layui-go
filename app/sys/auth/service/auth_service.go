package auth_service

import (
	"gorm.io/gorm"
	ad "pear-admin-layui-go/app/sys/auth/domain"
	rad "pear-admin-layui-go/app/sys/role_auth/domain"
	urd "pear-admin-layui-go/app/sys/user_role/domain"
	fd "pear-admin-layui-go/framework/database"
)

func RoleTree(id int64) ([]rad.RoleAuthTreeResponse, *gorm.DB) {
	//var auths []ad.Auth
	//tx := fd.Db.Model(ad.Auth{}).Where(map[string]interface{}{
	//	"del_flag": false,
	//	"enable":   true,
	//}).Select("id", "pid", "name").Order("sort").Find(&auths)
	//return auths, tx
	var ratr []rad.RoleAuthTreeResponse
	tx := fd.Db.Model(&ad.Auth{}).Select("sys_auth.id, sys_auth.name, sys_auth.pid, sys_role_auth.role_id").Joins("left join sys_role_auth on sys_role_auth.auth_id = sys_auth.id AND sys_role_auth.role_id = ?", id).Scan(&ratr)
	return ratr, tx
}

func EditPage(id int64) ad.Auth {
	var auth = ad.Auth{Id: id}
	fd.Db.Select("name", "type", "url", "icon", "code", "sort", "remark").First(&auth)
	return auth
}

func Enable(aer *ad.AuthEnableRequest) *gorm.DB {
	return fd.Db.Model(&ad.Auth{Id: aer.Id}).Select("enable", "update_time", "update_name",
		"update_by").Updates(&ad.Auth{Enable: aer.Enable, UpdateBy: aer.UpdateBy, UpdateName: aer.UpdateName, UpdateTime: aer.UpdateTime})
}

func Tree(id int64) ([]urd.UserRoleAuthTreeResponse, *gorm.DB) {
	var uratr []urd.UserRoleAuthTreeResponse
	//var auths []ad.Auth
	//tx := fd.Db.Model(ad.Auth{}).Where(map[string]interface{}{
	//	"del_flag": false,
	//	"enable":   true,
	//}).Select("id", "pid", "name", "icon", "type", "url").Order("sort").Find(&auths)
	//return auths, tx
	tx := fd.Db.Model(ad.Auth{}).Distinct("sys_auth.id", "sys_auth.pid", "sys_auth.name", "sys_auth.icon", "sys_auth.type", "sys_auth.url",
		"sys_auth.sort").Select("sys_auth.id, sys_auth.pid, sys_auth.name, sys_auth.icon, sys_auth.type, sys_auth.url, sys_auth.sort").Joins("INNER JOIN sys_role_auth ON sys_role_auth.auth_id = sys_auth.id "+
		"INNER JOIN sys_user_role ON sys_user_role.role_id = sys_role_auth.role_id").Where("sys_user_role.user_id = ?", id).Order("sys_auth.sort").Scan(&uratr)
	return uratr, tx
}

func AuthEdit(aar *ad.AuthEditRequest) (tx *gorm.DB) {
	return fd.Db.Model(ad.Auth{Id: aar.Id}).Select("name", "url", "code", "type", "sort", "icon", "remark",
		"update_time", "update_name", "update_name").Updates(ad.Auth{
		Name:       aar.Name,
		Url:        aar.Url,
		Code:       aar.Code,
		Type:       aar.Type,
		Sort:       aar.Sort,
		Icon:       aar.Icon,
		Remark:     aar.Remark,
		UpdateTime: aar.UpdateTime,
		UpdateBy:   aar.UpdateBy,
		UpdateName: aar.UpdateName,
	})
}

func AuthAdd(aar *ad.AuthAddRequest) (ad.Auth, error) {
	record := ad.Auth{
		Pid:        aar.Pid,
		Name:       aar.Name,
		Url:        aar.Url,
		Code:       aar.Code,
		Type:       aar.Type,
		Sort:       aar.Sort,
		Icon:       aar.Icon,
		Enable:     aar.Enable,
		Remark:     aar.Remark,
		DelFlag:    false,
		CreateTime: aar.CreateTime,
		CreateBy:   aar.CreateBy,
		CreateName: aar.CreateName,
	}
	result := fd.Db.Create(&record)
	return record, result.Error
}

func Table(asr *ad.AuthSearchRequest) ([]ad.Auth, int64) {
	var count int64
	db := fd.Db.Model(ad.Auth{}).Where("del_flag = false")
	if asr.Name != "" {
		db.Where("name like ?", "%"+asr.Name+"%")
	}
	//db.Count(&count)
	var auths []ad.Auth
	db.Select("id", "pid", "name", "url", "type", "sort", "icon", "code", "enable", "remark", "create_time",
		"create_name", "update_time", "update_name").Order("sort").Find(&auths)
	return auths, count
}

func UserAuths() []ad.Auth {
	var userAuths []ad.Auth
	fd.Db.Model(ad.Auth{}).Find(&userAuths)
	return userAuths
}
