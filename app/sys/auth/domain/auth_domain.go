package auth_domain

import ft "pear-admin-layui-go/framework/table"

type AuthEnableRequest struct {
	Id         int64 `form:"id"`
	Enable     bool  `form:"enable"`
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

type Auth struct {
	Id         int64
	Pid        int64
	Name       string
	Url        string
	Type       int
	Sort       int
	Icon       string
	Code       string
	OpenType   string
	Enable     bool
	Remark     string
	DelFlag    bool
	CreateTime int64
	CreateBy   int64
	CreateName string
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

func (Auth) TableName() string {
	return "sys_auth"
}

type AuthEditRequest struct {
	Id         int64  `form:"id"`
	Name       string `form:"name"`
	Url        string `form:"url"`
	Code       string `form:"code"`
	Type       int    `form:"type"`
	Sort       int    `form:"sort"`
	Icon       string `form:"icon"`
	Remark     string `form:"remark"`
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

type AuthAddRequest struct {
	Pid        int64  `form:"pid"`
	Name       string `form:"name"`
	Url        string `form:"url"`
	Code       string `form:"code"`
	Type       int    `form:"type"`
	Sort       int    `form:"sort"`
	Icon       string `form:"icon"`
	Enable     bool   `form:"enable"`
	Remark     string `form:"remark"`
	CreateTime int64
	CreateBy   int64
	CreateName string
}

type AuthTableResponse struct {
	Id         int64  `json:"id"`
	Pid        int64  `json:"pid"`
	Name       string `json:"name"`
	Url        string `json:"url"`
	Type       int    `json:"type"`
	Sort       int    `json:"sort"`
	Icon       string `json:"icon"`
	Code       string `json:"code"`
	Enable     bool   `json:"enable"`
	Remark     string `json:"remark"`
	CreateTime int64  `json:"createTime"`
	CreateName string `json:"createName"`
	UpdateTime int64  `json:"updateTime"`
	UpdateName string `json:"updateName"`
}

type AuthSearchRequest struct {
	ft.TableRequest
	Name string `form:"name"`
}

type AuthPage struct {
	Id       int64
	Pid      int64
	Name     string
	Url      string
	Type     int
	Icon     string
	ThisPage bool
	Childs   []AuthPage
}
