package role_domain

import ft "pear-admin-layui-go/framework/table"

type RoleAuthEditRequest struct {
	Id         int64 `form:"id"`
	AuthId     int64 `form:"authId"`
	Checked    bool  `form:"checked"`
	CreateTime int64
	CreateBy   int64
	CreateName string
}

type Role struct {
	Id         int64
	Name       string
	Code       string
	Enable     bool
	DelFlag    bool
	Remark     string
	CreateTime int64
	CreateBy   int64
	CreateName string
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

func (Role) TableName() string {
	return "sys_role"
}

type RoleTableRequest struct {
	ft.TableRequest
	Name string `form:"name"`
}

type RoleTableResponse struct {
	Id         int64  `json:"id"`
	Name       string `json:"name"`
	Code       string `json:"code"`
	Enable     bool   `json:"enable"`
	Remark     string `json:"remark"`
	CreateTime int64  `json:"createTime"`
	CreateName string `json:"createName"`
	UpdateTime int64  `json:"updateTime"`
	UpdateName string `json:"updateName"`
}

type RoleSaveRequest struct {
	Name       string `form:"name"`
	Code       string `form:"code"`
	Enable     bool   `form:"enable"`
	Remark     string `form:"remark"`
	CreateTime int64
	CreateBy   int64
	CreateName string
}

type RoleEditRequest struct {
	Id         int64  `form:"id"`
	Name       string `form:"name"`
	Code       string `form:"code"`
	Remark     string `form:"remark"`
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

type RoleEnableRequest struct {
	Id         int64 `form:"id"`
	Enable     bool  `form:"enable"`
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}
