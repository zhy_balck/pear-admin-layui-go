package user_role_domain

type UserRole struct {
	Id         int64
	UserId     int64
	RoleId     int64
	Type       int
	Status     int
	Remark     string
	DelFlag    bool
	CreateTime int64
	CreateBy   int64
	CreateName string
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

func (UserRole) TableName() string {
	return "sys_user_role"
}

type RoleUserCheckboxResponse struct {
	RoleId   int64
	RoleName string
	UserId   int64
}

type UserRoleAuthTreeResponse struct {
	Id   int64
	Pid  int64
	Name string
	Icon string
	Type int
	Url  string
}
