package dict_domain

import ft "pear-admin-layui-go/framework/table"

type Dict struct {
	Id         int
	TypeName   string
	TypeCode   string
	Remark     string
	Enable     bool
	DelFlag    bool
	CreateTime int64
	CreateBy   int64
	CreateName string
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

func (Dict) TableName() string {
	return "sys_dict"
}

type DictSearchRequest struct {
	ft.TableRequest
	TypeName string `form:"typeName"`
}

type DictTableResponse struct {
	Id         int    `json:"id"`
	TypeName   string `json:"typeName"`
	TypeCode   string `json:"typeCode"`
	Enable     bool   `json:"enable"`
	Remark     string `json:"remark"`
	CreateTime int64  `json:"createTime"`
	CreateBy   int64  `json:"createBy"`
	CreateName string `json:"createName"`
	UpdateTime int64  `json:"updateTime"`
	UpdateBy   int64  `json:"updateBy"`
	UpdateName string `json:"updateName"`
}

type DictEnableRequest struct {
	Id         int  `form:"id"`
	Enable     bool `form:"enable"`
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

type DictSaveRequest struct {
	TypeName string `form:"typeName"`
	TypeCode string `form:"typeCode"`
	Remark   string `form:"remark"`
	// -------------------
	CreateTime int64
	CreateBy   int64
	CreateName string
}

type DictEditRequest struct {
	Id         int    `form:"id"`
	TypeName   string `form:"typeName"`
	TypeCode   string `form:"typeCode"`
	Remark     string `form:"remark"`
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}
