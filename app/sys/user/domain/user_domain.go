package user_domain

import ft "pear-admin-layui-go/framework/table"

type UserEnableRequest struct {
	Id         int64 `form:"id"`
	Enable     bool  `form:"enable"`
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

type LoginRequest struct {
	Username string `form:"username"`
	Password string `form:"password"`
}

type UserSaveRequest struct {
	Username   string `form:"username"`
	NickName   string `form:"nickName"`
	RealName   string `form:"realName"`
	Sex        int    `form:"sex"`
	Phone      string `form:"phone"`
	Email      string `form:"email"`
	Enable     bool   `form:"enable"`
	Remark     string `form:"remark"`
	CreateTime int64
	CreateBy   int64
	CreateName string
}

type UserEditRequest struct {
	Id         int64  `form:"id"`
	Username   string `form:"username"`
	NickName   string `form:"nickName"`
	RealName   string `form:"realName"`
	Sex        int    `form:"sex"`
	Phone      string `form:"phone"`
	Email      string `form:"email"`
	Remark     string `form:"remark"`
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

type User struct {
	Id         int64
	RealName   string
	NickName   string
	Username   string
	Password   string
	Enable     bool
	Phone      string
	Sex        int
	Email      string
	Login      int
	Remark     string
	DelFlag    bool
	CreateTime int64
	CreateBy   int64
	CreateName string
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

func (User) TableName() string {
	return "sys_user"
}

type UserRoleEditRquest struct {
	Id         int64 `form:"id"`
	RoleId     int64 `form:"roleId"`
	Checked    bool  `form:"checked"`
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

type UserTableResponse struct {
	Id         int64  `json:"id"`
	Username   string `json:"username"`
	RealName   string `json:"realName"`
	NickName   string `json:"nickName"`
	Enable     bool   `json:"enable"`
	Phone      string `json:"phone"`
	Sex        int    `json:"sex"`
	Email      string `json:"email"`
	Login      int    `json:"login"`
	Remark     string `json:"remark"`
	CreateTime int64  `json:"createTime"`
	CreateName string `json:"createName"`
	UpdateTime int64  `json:"updateTime"`
	UpdateName string `json:"updateName"`
}

type UserSearchRequest struct {
	ft.TableRequest
	Username string `form:"username"`
}
