package dept_domain

import ft "pear-admin-layui-go/framework/table"

type DeptEditRequest struct {
	Id         int64  `form:"id"`
	Name       string `form:"name"`
	Location   string `form:"location"`
	Remark     string `form:"remark"`
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

type DeptTableResponse struct {
	Id         int64  `json:"id"`
	Name       string `json:"name"`
	Leader     int64  `json:"leader"`
	LeaderName string `json:"LeaderName"`
	Pid        int64  `json:"pid"`
	Location   string `json:"location"`
	Number     int    `json:"number"`
	Remark     string `json:"remark"`
	CreateTime int64  `json:"createTime"`
	CreateName string `json:"createName"`
	UpdateTime int64  `json:"updateTime"`
	UpdateName string `json:"updateName"`
}

type DeptSaveRequest struct {
	Name       string `form:"name"`
	Pid        int64  `form:"pid"`
	Location   string `form:"location"`
	Remark     string `form:"remark"`
	CreateTime int64
	CreateBy   int64
	CreateName string
}

type DeptTableRequest struct {
	ft.TableRequest
	Name string `form:"name"`
}

type Dept struct {
	Id         int64
	Name       string
	Leader     int64
	LeaderName string
	Pid        int64
	Location   string
	Number     int
	Type       int
	Status     int
	Remark     string
	DelFlag    bool
	CreateTime int64
	CreateBy   int64
	CreateName string
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

func (Dept) TableName() string {
	return "sys_dept"
}
