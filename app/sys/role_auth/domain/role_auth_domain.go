package role_auth_domain

type RoleAuth struct {
	Id         int64
	RoleId     int64
	AuthId     int64
	Remark     string
	DelFlag    bool
	CreateTime int64
	CreateBy   int64
	CreateName string
	UpdateTime int64
	UpdateBy   int64
	UpdateName string
}

func (RoleAuth) TableName() string {
	return "sys_role_auth"
}

type RoleAuthTreeResponse struct {
	Id     int64
	Pid    int64
	Name   string
	RoleId int64
}
