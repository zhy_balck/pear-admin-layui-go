package role_auth_service

import (
	"gorm.io/gorm"
	rd "pear-admin-layui-go/app/sys/role/domain"
	rad "pear-admin-layui-go/app/sys/role_auth/domain"
	fd "pear-admin-layui-go/framework/database"
)

func RoleAuthEdit(raer *rd.RoleAuthEditRequest) *gorm.DB {
	if raer.Checked {
		// 新增
		return fd.Db.Create(&rad.RoleAuth{RoleId: raer.Id, AuthId: raer.AuthId, CreateTime: raer.CreateTime, CreateName: raer.CreateName, CreateBy: raer.CreateBy})
	} else {
		// 删除
		return fd.Db.Where("role_id = ? AND auth_id = ?", raer.Id, raer.AuthId).Delete(rad.RoleAuth{})
	}
}

func SaveRoleAuth(roleAuths []rad.RoleAuth) (int, int) {
	fd.Db.Where("role_id = ?", roleAuths[0].RoleId).Delete(&rad.RoleAuth{})
	count := len(roleAuths)
	tx := fd.Db.Create(&roleAuths)
	return int(tx.RowsAffected), count
}
