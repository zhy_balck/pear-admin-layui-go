package config

import (
	"github.com/BurntSushi/toml"
	"log"
)

var Cfg Config

type Config struct {
	Application Application
}

type Application struct {
	Name    string
	Version string
}

func Load() Config {
	var c Config
	_, e := toml.DecodeFile("./config/config.toml", &c)
	if e != nil {
		log.Fatalln(e)
	}
	Cfg = c
	return c
}
