package main

import (
	"encoding/gob"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"pear-admin-layui-go/config"
	fc "pear-admin-layui-go/framework/core"
	"pear-admin-layui-go/framework/login_filter"
	fs "pear-admin-layui-go/framework/session"
	"pear-admin-layui-go/routes"
)

func main() {
	// 存放与session中的结构体需要注册
	gob.Register(login_filter.UserSession{})

	// 加载toml配置
	cfg := config.Load()
	log.Println("Config = ", cfg)

	e := fc.Start()

	e.Delims("{[{", "}]}")
	// 配置模板路径和静态文件路径 皆以main.go入口文件的相对路径为准
	e.LoadHTMLGlob("views/**/*/*")

	e.Static("/pear-admin-layui", "static/pear-admin-layui")
	e.StaticFile("/favicon.ico", "static/favicon.ico")

	// session 中间件
	e.Use(sessions.Sessions("server_session", fs.Store))

	// 所有日志拦截
	//e.Use(log2.WebLog())

	// 登录页
	e.GET("/login", func(c *gin.Context) {
		c.HTML(http.StatusOK, "sys/login/login.html", gin.H{
			"title": cfg.Application.Name,
		})
	})

	// 登录拦截 在这个之后注册的所有URL都会被拦截
	e.Use(login_filter.Authority())

	// 配置路由
	routes.SetupRouter(e)

	_ = e.Run(":9999")
}
