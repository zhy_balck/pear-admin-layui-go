package framework_database

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var (
	Db *gorm.DB
)

func init() {
	dsn := "root:zhy_black.@tcp(127.0.0.1:3306)/go_fast"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	Db = db
	if err != nil {
		panic("failed to connect database")
	}
}
