package framework_table

type TableResponse struct {
	Code  int         `json:"code"`
	Msg   string      `json:"msg"`
	Count int64       `json:"count"`
	Data  interface{} `json:"data"`
}

type TableRequest struct {
	Page  int `form:"page"`
	Limit int `form:"limit"`
}
