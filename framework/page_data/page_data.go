package framework_page_data

import (
	"github.com/gin-gonic/gin"
	ad "pear-admin-layui-go/app/sys/auth/domain"
	"pear-admin-layui-go/config"
)

func PageParam(c *gin.Context) map[string]interface{} {
	//cu, e := login_filter.GetCurrentUser(c)
	//if e != nil {
	//	cu = login_filter.UserSession{}
	//}
	//
	//auths := as.UserAuths()
	//thisPathURL := c.Request.URL.Path
	//authPages, navs := authScheme(auths, thisPathURL)
	//last := len(navs) - 1

	return map[string]interface{}{
		//"currentUser": cu,
		"title": config.Cfg.Application.Name,
		//"version":     config.Cfg.Application.Version,
		//"auths":       authPages,
		//"navs":        navs,
		//"last":        last,
	}
}

func authScheme(auths []ad.Auth, thisPathURL string) ([]ad.AuthPage, []string) {
	var aps []ad.AuthPage
	var navs []string
	var childsMap = make(map[int64][]ad.AuthPage)
	for _, v := range auths {
		var thisPage bool
		if v.Url == thisPathURL {
			thisPage = true
		}
		if v.Pid == 0 {
			aps = append(aps, ad.AuthPage{
				Id:       v.Id,
				Pid:      v.Pid,
				Name:     v.Name,
				Url:      v.Url,
				Type:     v.Type,
				Icon:     v.Icon,
				ThisPage: thisPage,
			})
		} else {
			childsMap[v.Pid] = append(childsMap[v.Pid], ad.AuthPage{
				Id:       v.Id,
				Pid:      v.Pid,
				Name:     v.Name,
				Url:      v.Url,
				Type:     v.Type,
				Icon:     v.Icon,
				ThisPage: thisPage,
			})
		}
	}
	if len(childsMap) > 0 {
		flag := false
		for i, v := range aps {
			aps[i].Childs = childsMap[v.Id]
			if v.ThisPage {
				navs = append(navs, aps[i].Name)
			}
			if !flag {
				for _, v := range aps[i].Childs {
					if v.ThisPage {
						aps[i].ThisPage = true
						navs = append(navs, aps[i].Name, v.Name)
						flag = true
						break
					}
				}
			}
		}
	}
	return aps, navs
}
