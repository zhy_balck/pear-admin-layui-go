package framework_core

import "github.com/gin-gonic/gin"

var Eng *gin.Engine

func Start() *gin.Engine {
	Eng = gin.Default()
	return Eng
}
