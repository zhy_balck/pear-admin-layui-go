package routes

import (
	"github.com/gin-gonic/gin"
	routes "pear-admin-layui-go/routes/sys"
)

func SetupRouter(e *gin.Engine) {
	routes.RoleRegister(e)
	routes.UserRegister(e)
	routes.AuthRegister(e)
	routes.ConfigRegister(e)
	routes.DictRegister(e)
	routes.DictListRegister(e)
	routes.FrameworkRegister(e)
	routes.LogRegister(e)
	routes.DeptRegister(e)
}
