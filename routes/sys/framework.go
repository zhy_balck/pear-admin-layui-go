package routes

import (
	"github.com/gin-gonic/gin"
	"html/template"
	"log"
	"net/http"
	"pear-admin-layui-go/config"
	"pear-admin-layui-go/framework/login_filter"
)

func FrameworkRegister(e *gin.Engine) {
	// 首页
	e.GET("/", func(c *gin.Context) {
		cu, _ := login_filter.GetCurrentUser(c)
		log.Println(cu)
		c.HTML(http.StatusOK, "sys/index/index.html", gin.H{
			"title": config.Cfg.Application.Name,
		})
	})

	e.GET("/sys/console", func(c *gin.Context) {
		c.HTML(http.StatusOK, "sys/console/console.html", gin.H{})
	})

	e.GET("/demo/console/console2", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/console/console2.html", gin.H{})
	})

	e.GET("/demo/system/theme", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/system/theme.html", gin.H{
			"enable": template.HTML("<input type=\"checkbox\" name=\"enable\" value=\"{{d.id}}\" lay-skin=\"switch\" lay-text=\"启用|禁用\"\n           lay-filter=\"user-enable\" checked=\"{{ d.enable == 0 ? 'true' : 'false' }}\">"),
		})
	})

	e.GET("/demo/document/core", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/core.html", gin.H{})
	})

	e.GET("/demo/document/button", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/button.html", gin.H{})
	})

	e.GET("/demo/document/form", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/form.html", gin.H{})
	})

	e.GET("/demo/document/icon", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/icon.html", gin.H{})
	})

	e.GET("/demo/document/select", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/select.html", gin.H{})
	})

	e.GET("/demo/document/tag", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/tag.html", gin.H{})
	})

	e.GET("/demo/document/table", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/table.html", gin.H{
			"enable": template.HTML("<input type=\"checkbox\" name=\"enable\" value=\"{{d.id}}\" lay-skin=\"switch\" lay-text=\"启用|禁用\"\n           lay-filter=\"user-enable\"\n           checked=\"{{ d.enable == 0 ? 'true' : 'false' }}\">"),
		})
	})

	e.GET("/demo/document/step", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/step.html", gin.H{})
	})

	e.GET("/demo/document/treetable", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/treetable.html", gin.H{
			"enable": template.HTML("<input type=\"checkbox\" name=\"enable\" value=\"{{d.id}}\" lay-skin=\"switch\" lay-text=\"启用|禁用\"\n           lay-filter=\"user-enable\" checked=\"{{ d.id == 10003 ? 'true' : 'false' }}\">"),
		})
	})

	e.GET("/demo/document/dtree", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/dtree.html", gin.H{})
	})

	e.GET("/demo/document/tinymce", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/tinymce.html", gin.H{})
	})

	e.GET("/demo/document/card", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/card.html", gin.H{})
	})

	e.GET("/demo/document/drawer", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/drawer.html", gin.H{})
	})

	e.GET("/demo/document/notice", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/notice.html", gin.H{})
	})

	e.GET("/demo/document/toast", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/toast.html", gin.H{})
	})

	e.GET("/demo/document/loading", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/loading.html", gin.H{})
	})

	e.GET("/demo/document/popup", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/popup.html", gin.H{})
	})

	e.GET("/demo/document/tab", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/tab.html", gin.H{})
	})

	e.GET("/demo/document/space", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/space.html", gin.H{})
	})

	e.GET("/demo/document/menu", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/menu.html", gin.H{})
	})

	e.GET("/demo/document/encrypt", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/encrypt.html", gin.H{})
	})

	e.GET("/demo/document/iconPicker", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/iconPicker.html", gin.H{})
	})

	e.GET("/demo/document/area", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/area.html", gin.H{})
	})

	e.GET("/demo/document/count", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/count.html", gin.H{})
	})

	e.GET("/demo/document/topBar", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/topBar.html", gin.H{})
	})

	e.GET("/demo/document/watermark", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/watermark.html", gin.H{})
	})

	e.GET("/demo/document/fullscreen", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/fullscreen.html", gin.H{})
	})

	e.GET("/demo/document/popover", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/document/popover.html", gin.H{})
	})

	e.GET("/demo/result/success", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/result/success.html", gin.H{})
	})

	e.GET("/demo/result/error", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/result/error.html", gin.H{})
	})

	e.GET("/demo/error/403", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/error/403.html", gin.H{})
	})

	e.GET("/demo/error/404", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/error/404.html", gin.H{})
	})

	e.GET("/demo/error/500", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/error/500.html", gin.H{})
	})

	e.GET("/demo/system/login", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/system/login.html", gin.H{})
	})

	e.GET("/demo/system/space", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/system/space.html", gin.H{})
	})

	e.GET("/demo/echarts/line", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/echarts/line.html", gin.H{})
	})

	e.GET("/demo/echarts/column", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/echarts/column.html", gin.H{})
	})

	e.GET("/demo/code/index", func(c *gin.Context) {
		c.HTML(http.StatusOK, "demo/code/index.html", gin.H{})
	})
}
