package routes

import (
	"github.com/gin-gonic/gin"
	dc "pear-admin-layui-go/app/sys/dept/controller"
)

func DeptRegister(e *gin.Engine) {
	e.GET("/sys/dept", dc.Dept)
	e.GET("/sys/dept/add", dc.AddPage)
	e.GET("/sys/dept/edit/:id", dc.EditPage)
	e.POST("/sys/dept/table", dc.Table)
	e.POST("/sys/dept/save", dc.Save)
	e.POST("/sys/dept/edit", dc.Edit)
	e.POST("/sys/dept/tree", dc.Tree)
}
