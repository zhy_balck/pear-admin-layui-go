package routes

import (
	"github.com/gin-gonic/gin"
	"pear-admin-layui-go/app/sys/role/controller"
)

func RoleRegister(e *gin.Engine) {
	e.GET("/sys/role", role_controller.Page)
	e.GET("/sys/role/add", role_controller.AddPage)
	e.GET("/sys/role/edit/:id", role_controller.EditPage)
	e.POST("/sys/role/table", role_controller.Table)
	e.POST("/sys/role/save", role_controller.Save)
	e.POST("/sys/role/enable", role_controller.Enable)
	e.POST("/sys/role/edit", role_controller.Edit)
	e.GET("/sys/role/auth/:id", role_controller.AuthPage)
	e.POST("/sys/role/auth/save", role_controller.AuthSave)
	e.POST("/sys/role/auth/edit", role_controller.RoleAuthEdit)
}
