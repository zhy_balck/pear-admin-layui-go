package routes

import (
	"github.com/gin-gonic/gin"
	lc "pear-admin-layui-go/app/sys/log/controller"
)

func LogRegister(e *gin.Engine) {
	e.GET("/sys/log", lc.LogPage)
	e.POST("/sys/log/table", lc.Table)
}
