package routes

import (
	"github.com/gin-gonic/gin"
	dc "pear-admin-layui-go/app/sys/dict/controller"
)

func DictRegister(e *gin.Engine) {
	e.GET("/sys/dict", dc.Page)
	e.GET("/sys/dict/add", dc.AddPage)
	e.POST("/sys/dict/enable", dc.Enable)
	e.POST("/sys/dict/table", dc.Table)
	e.POST("/sys/dict/save", dc.Save)
	e.POST("/sys/dict/edit", dc.Edit)
	e.GET("/sys/dict/edit/:id", dc.EditPage)
}
