package routes

import (
	"github.com/gin-gonic/gin"
	ac "pear-admin-layui-go/app/sys/auth/controlle"
)

func AuthRegister(e *gin.Engine) {
	e.GET("/sys/auth", ac.Auth)
	e.GET("/sys/auth/add", ac.AddPage)
	e.GET("/sys/auth/edit/:id", ac.EditPage)
	e.POST("/sys/auth/table", ac.Table)
	e.POST("/sys/auth/enable", ac.Enable)
	e.POST("/sys/auth/save", ac.AuthAdd)
	e.POST("/sys/auth/edit", ac.AuthEdit)
	e.POST("/sys/auth/tree", ac.Tree)
}
