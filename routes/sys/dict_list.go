package routes

import (
	"github.com/gin-gonic/gin"
	dlc "pear-admin-layui-go/app/sys/dict_list/controller"
)

func DictListRegister(e *gin.Engine) {
	e.GET("/sys/dict_list/add/:typeCode", dlc.AddPage)
	e.GET("/sys/dict_list/edit/:id", dlc.EditPage)
	e.POST("/sys/dict_list/table", dlc.Table)
	e.POST("/sys/dict_list/save", dlc.Save)
	e.POST("/sys/dict_list/edit", dlc.Edit)
	e.POST("/sys/dict_list/enable", dlc.Enable)
	e.POST("/sys/dict_list/isDefault", dlc.IsDefault)
}
