package routes

import (
	"github.com/gin-gonic/gin"
	cc "pear-admin-layui-go/app/sys/config/controller"
)

func ConfigRegister(e *gin.Engine) {
	e.GET("/sys/config", cc.Page)
	e.GET("/sys/config/add", cc.AddPage)
	e.GET("/sys/config/edit/:id", cc.EditPage)
	e.POST("/sys/config/save", cc.Save)
	e.POST("/sys/config/table", cc.Table)
	e.POST("/sys/config/edit", cc.Edit)
	e.POST("/sys/config/enable", cc.Enable)
}
