package routes

import (
	"github.com/gin-gonic/gin"
	"pear-admin-layui-go/app/sys/user/controller"
)

func UserRegister(e *gin.Engine) {
	e.GET("/sys/user", user_controller.User)
	e.GET("/sys/user/add", user_controller.AddPage)
	e.GET("/sys/user/edit/:id", user_controller.EditPage)
	e.POST("/login", user_controller.Login)
	e.POST("/logout", user_controller.Logout)
	e.POST("/sys/user/table", user_controller.Table)
	e.POST("/sys/user/save", user_controller.Save)
	e.POST("/sys/user/edit", user_controller.UserEdit)
	e.POST("/sys/user/enable", user_controller.Enable)
	e.GET("/sys/user/role/:id", user_controller.RoleEdit)
	e.POST("/sys/user/role/save", user_controller.UserRoleSave)
	e.POST("/sys/user/role/edit", user_controller.UserRoleEdit)
}
