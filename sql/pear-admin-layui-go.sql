/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80012 (8.0.12)
 Source Host           : localhost:3306
 Source Schema         : go_fast

 Target Server Type    : MySQL
 Target Server Version : 80012 (8.0.12)
 File Encoding         : 65001

 Date: 29/01/2023 09:30:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_auth
-- ----------------------------
DROP TABLE IF EXISTS `sys_auth`;
CREATE TABLE `sys_auth`  (
                             `id` int(11) NOT NULL AUTO_INCREMENT,
                             `pid` int(11) NULL DEFAULT NULL,
                             `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `type` int(11) NULL DEFAULT NULL,
                             `sort` int(11) NULL DEFAULT NULL,
                             `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `enable` tinyint(1) NULL DEFAULT NULL,
                             `open_type` varbinary(255) NULL DEFAULT NULL,
                             `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `del_flag` tinyint(1) NULL DEFAULT 0,
                             `create_time` int(11) NULL DEFAULT NULL,
                             `create_by` int(11) NULL DEFAULT NULL,
                             `create_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `update_time` int(11) NULL DEFAULT NULL,
                             `update_by` int(11) NULL DEFAULT NULL,
                             `update_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 72 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_auth
-- ----------------------------
INSERT INTO `sys_auth` VALUES (13, 0, '常用页面', '', 0, 5, 'layui-icon-component', 'index:template', 1, '', '', 0, 1670580441, 1, 'admin', 1670657608, 1, 'admin');
INSERT INTO `sys_auth` VALUES (12, 0, '错误页面', '', 0, 3, 'layui-icon-face-cry', 'index:error', 1, '', '', 0, 1670580358, 1, 'admin', 1670657585, 0, 'admin');
INSERT INTO `sys_auth` VALUES (11, 0, '结果页面', '', 0, 2, 'layui-icon-auz', 'index:result', 1, '', '结果页面', 0, 1670580329, 1, 'admin', 1670657574, 0, 'admin');
INSERT INTO `sys_auth` VALUES (10, 0, '常用组件', '', 0, 1, 'layui-icon-component', 'index:component', 1, '', '常用组件', 0, 1670580232, 1, 'admin', 1670657563, 0, 'admin');
INSERT INTO `sys_auth` VALUES (1, 0, '工作空间', '', 0, 0, 'layui-icon-console', 'index:console', 1, '', '工作空间', 0, 1670579933, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (2, 0, '系统管理', '', 0, 4, 'layui-icon-set-fill', 'index:system:setting', 1, '', '系统管理', 0, 1670580075, 1, 'admin', 1670657599, 0, 'admin');
INSERT INTO `sys_auth` VALUES (14, 0, '数据图表', '', 0, 6, 'layui-icon-chart', 'index:chart', 1, '', '', 0, 1670580481, 1, 'admin', 1670657616, 0, 'admin');
INSERT INTO `sys_auth` VALUES (15, 0, '开发工具', '', 0, 7, 'layui-icon-util', 'index:tool', 1, '', '', 0, 1670580508, 1, 'admin', 1670657625, 0, 'admin');
INSERT INTO `sys_auth` VALUES (16, 1, '控制后台', '/sys/console', 1, 0, '', 'sys:console', 1, '', '控制后台', 0, 1670657873, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (17, 1, '数据分析', '/demo/console/console2', 1, 1, '', 'demo:console', 1, '', '数据分析', 0, 1670657927, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (18, 1, '百度一下', 'https://www.bing.com', 1, 2, '', 'console:baidu', 1, '', '', 0, 1670657959, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (19, 1, '主题预览', '/demo/system/theme', 1, 3, '', 'demo:sys:theme', 1, '', '', 0, 1670657995, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (20, 1, '酸爽翻倍', '/demo/document/core', 1, 4, '', 'demo:document:core', 1, '', '', 0, 1670658027, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (21, 10, '基础组件', '', 0, 0, '', '', 1, '', '', 0, 1670658073, 1, 'admin', 1670658116, 1, 'admin');
INSERT INTO `sys_auth` VALUES (22, 21, '功能按钮', '/demo/document/button', 1, 0, '', '', 1, '', '', 0, 1670658110, 1, 'admin', 1670658209, 0, 'admin');
INSERT INTO `sys_auth` VALUES (23, 21, '表单集合', '/demo/document/form', 1, 0, '', '', 1, '', '', 0, 1670658135, 1, 'admin', 1670658214, 1, 'admin');
INSERT INTO `sys_auth` VALUES (24, 21, '字体图标', '/demo/document/icon', 1, 0, '', '', 1, '', '', 0, 1670658233, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (25, 21, '多选下拉', '/demo/document/select', 1, 0, '', '', 1, '', '', 0, 1670658262, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (26, 21, '动态标签', '/demo/document/tag', 1, 0, '', '', 1, '', '', 0, 1670658467, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (27, 10, '进阶组件', '', 0, 0, '', '', 1, '', '', 0, 1670658492, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (28, 10, '弹层组件', '', 0, 0, '', '', 1, '', '', 0, 1670658540, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (29, 10, '高级组件', '', 0, 0, '', '', 1, '', '', 0, 1670658549, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (30, 10, '其他组件', '', 0, 0, '', '', 1, '', '', 0, 1670658557, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (31, 27, '数据表格', '/demo/document/table', 1, 0, '', '', 1, '', '', 0, 1670658579, 1, 'admin', 1670658591, 0, 'admin');
INSERT INTO `sys_auth` VALUES (32, 27, '分布表单', '/demo/document/step', 1, 0, '', '', 1, '', '', 0, 1670658608, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (33, 27, '树形表格', '/demo/document/treetable', 1, 0, '', '', 1, '', '', 0, 1670658631, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (34, 27, '树状结构', '/demo/document/dtree', 1, 0, '', '', 1, '', '', 0, 1670658648, 1, 'admin', 1670658668, 0, 'admin');
INSERT INTO `sys_auth` VALUES (35, 27, '文本编辑', '/demo/document/tinymce', 1, 0, '', '', 1, '', '', 0, 1670658662, 1, 'admin', 1670658675, 0, 'admin');
INSERT INTO `sys_auth` VALUES (36, 27, '卡片组件', '/demo/document/card', 1, 0, '', '', 1, '', '', 0, 1670658691, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (37, 28, '抽屉组件', '/demo/document/drawer', 1, 0, '', '', 1, '', '', 0, 1670661626, 1, 'admin', 1670661731, 0, 'admin');
INSERT INTO `sys_auth` VALUES (38, 28, '消息通知 (过时)', '/demo/document/notice', 1, 0, '', '', 1, '', '', 0, 1670661647, 1, 'admin', 1670661725, 0, 'admin');
INSERT INTO `sys_auth` VALUES (39, 28, '消息通知 (新增)', '/demo/document/toast', 1, 0, '', '', 1, '', '', 0, 1670661667, 1, 'admin', 1670661717, 0, 'admin');
INSERT INTO `sys_auth` VALUES (40, 28, '加载组件', '/demo/document/loading', 1, 0, '', '', 1, '', '', 0, 1670661682, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (41, 28, '弹层组件', '/demo/document/popup', 1, 0, '', '', 1, '', '', 0, 1670661697, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (42, 29, '多选项卡', '/demo/document/tab', 1, 0, '', '', 1, '', '', 0, 1670661750, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (43, 29, '数据菜单', '', 1, 0, '', '/demo/document/menu', 1, '', '', 0, 1670661763, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (44, 30, '哈希加密', '/demo/document/encrypt', 1, 0, '', '', 1, '', '', 0, 1670661783, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (45, 30, '图标选择', '/demo/document/iconPicker', 1, 0, '', '', 1, '', '', 0, 1670661795, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (46, 30, '省市级联', '/demo/document/area', 1, 0, '', '', 1, '', '', 0, 1670661812, 1, 'admin', 1670661882, 0, 'admin');
INSERT INTO `sys_auth` VALUES (47, 30, '数字滚动', '/demo/document/count', 1, 0, '', '', 1, '', '', 0, 1670661826, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (48, 30, '顶部返回', '/demo/document/topBar', 1, 0, '', '', 1, '', '', 0, 1670661838, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (49, 30, '水印组件', '/demo/document/watermark', 1, 0, '', '', 1, '', '', 0, 1670661849, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (50, 30, '全屏组件', '/demo/document/fullscreen', 1, 0, '', '', 1, '', '', 0, 1670661861, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (51, 30, '汽泡组件', '/demo/document/popover', 1, 0, '', '', 1, '', '', 0, 1670661872, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (52, 11, '成功', '/demo/result/success', 1, 0, '', '', 1, '', '', 0, 1670661907, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (53, 11, '失败', '/demo/result/error', 1, 0, '', '', 1, '', '', 0, 1670661918, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (54, 12, '403', '/demo/error/403', 1, 0, '', '', 1, '', '', 0, 1670661931, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (55, 12, '404', '/demo/error/404', 1, 0, '', '', 1, '', '', 0, 1670661941, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (56, 12, '500', '/demo/error/500', 1, 0, '', '', 1, '', '', 0, 1670661953, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (57, 2, '用户管理', '/sys/user', 1, 0, '', '', 1, '', '', 0, 1670661980, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (58, 2, '角色管理', '/sys/role', 1, 0, '', '', 1, '', '', 0, 1670661991, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (59, 2, '权限管理', '/sys/auth', 1, 0, '', '', 1, '', '', 0, 1670662003, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (60, 2, '部门管理', '/sys/dept', 1, 0, '', '', 1, '', '', 0, 1670662019, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (61, 2, '行为日志', '/sys/log', 1, 0, '', '', 1, '', '', 0, 1670662033, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (62, 2, '数据字典', '/sys/dict', 1, 0, '', '', 1, '', '', 0, 1670662044, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (63, 13, '登录页面', '/demo/system/login', 1, 0, '', '', 1, '', '', 0, 1670662064, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (64, 13, '空白页面', '/demo/system/space', 1, 0, '', '', 1, '', '', 0, 1670662076, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (65, 14, '折线图', '/demo/echarts/line', 1, 0, '', '', 1, '', '', 0, 1670662088, 1, 'admin', 1670662101, 0, 'admin');
INSERT INTO `sys_auth` VALUES (66, 14, '柱状图', '/demo/echarts/column', 1, 0, '', '', 1, '', '', 0, 1670662113, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (67, 15, '表单构建', '/demo/code/index', 1, 0, '', '', 1, '', '', 0, 1670662126, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (68, 15, 'gin', 'https://gin-gonic.com/zh-cn/docs/', 1, 0, '', '', 1, '', '', 0, 1670827230, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (69, 15, 'gorm', 'https://gorm.io/zh_CN/docs/query.html', 1, 0, '', '', 1, '', '', 0, 1670827264, 1, 'admin', 0, 0, '');
INSERT INTO `sys_auth` VALUES (70, 15, 'layui', 'https://layui.gitee.io/v2/docs/', 1, 0, '', '', 1, '', '', 0, 1670827285, 1, 'admin', 1670827292, 0, 'admin');
INSERT INTO `sys_auth` VALUES (71, 2, '参数管理', '/sys/config', 1, 0, '', '', 1, '', '', 0, 1671954056, 1, 'admin', 0, 0, '');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
                               `id` int(11) NOT NULL AUTO_INCREMENT,
                               `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                               `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                               `enable` tinyint(1) NULL DEFAULT NULL,
                               `type` int(11) NULL DEFAULT NULL,
                               `status` int(11) NULL DEFAULT NULL,
                               `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                               `del_flag` tinyint(1) NULL DEFAULT 0,
                               `create_time` int(11) NULL DEFAULT NULL,
                               `create_by` int(11) NULL DEFAULT NULL,
                               `create_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                               `update_time` int(11) NULL DEFAULT NULL,
                               `update_by` int(11) NULL DEFAULT NULL,
                               `update_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, 'a', 'b', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_config` VALUES (2, 'c', 'c', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_config` VALUES (3, 'cc', 'c', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_config` VALUES (4, 'xxx', 'xxx', 0, 0, 0, 'xxx', 0, 1671958465, 1, 'admin', 1671959355, 1, 'admin');
INSERT INTO `sys_config` VALUES (5, 'aaa', 'bvv', 1, 0, 0, 'xxx', 0, 1671958598, 1, 'admin', 1671960130, 1, 'admin');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
                             `id` int(11) NOT NULL AUTO_INCREMENT,
                             `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `pid` int(11) NULL DEFAULT NULL,
                             `leader` int(11) NULL DEFAULT NULL,
                             `leader_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `number` int(11) NULL DEFAULT NULL,
                             `type` int(11) NULL DEFAULT NULL,
                             `status` int(11) NULL DEFAULT NULL,
                             `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `del_flag` tinyint(1) NULL DEFAULT 0,
                             `create_time` int(11) NULL DEFAULT NULL,
                             `create_by` int(11) NULL DEFAULT NULL,
                             `create_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `update_time` int(11) NULL DEFAULT NULL,
                             `update_by` int(11) NULL DEFAULT NULL,
                             `update_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1, '哒一扑爬', 0, 0, '', '哒一扑爬', 0, 0, 0, '1', 0, 1670924490, 1, 'admin', 1670926252, 1, 'admin');
INSERT INTO `sys_dept` VALUES (2, '重庆分公司', 1, 0, '', '重庆分公司', 0, 0, 0, '重庆分公司', 0, 1672196810, 1, 'admin', 0, 0, '');
INSERT INTO `sys_dept` VALUES (3, '财务部', 2, 0, '', '财务部', 0, 0, 0, '', 0, 1672196961, 1, 'admin', 0, 0, '');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
                             `id` int(11) NOT NULL AUTO_INCREMENT,
                             `type_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `type_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `enable` tinyint(1) NULL DEFAULT NULL,
                             `del_flag` tinyint(1) NULL DEFAULT 0,
                             `create_time` int(11) NULL DEFAULT NULL,
                             `create_by` int(11) NULL DEFAULT NULL,
                             `create_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `update_time` int(11) NULL DEFAULT NULL,
                             `update_by` int(11) NULL DEFAULT NULL,
                             `update_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES (1, '用户性别', 'user_sex', '用户性别', 1, 0, 1670409825, 1, 'admin', 1670471268, 1, 'admin');
INSERT INTO `sys_dict` VALUES (2, '权限类型', 'auth_type', '权限类型', 1, 0, 1670575435, 1, 'admin', 0, 0, '');

-- ----------------------------
-- Table structure for sys_dict_list
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_list`;
CREATE TABLE `sys_dict_list`  (
                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `type_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                  `data_label` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                  `data_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                  `is_default` tinyint(1) NULL DEFAULT NULL,
                                  `enable` tinyint(1) NULL DEFAULT NULL,
                                  `del_flag` tinyint(1) NULL DEFAULT 0,
                                  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                  `create_time` int(11) NULL DEFAULT NULL,
                                  `create_by` int(11) NULL DEFAULT NULL,
                                  `create_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                  `update_time` int(11) NULL DEFAULT NULL,
                                  `update_by` int(11) NULL DEFAULT NULL,
                                  `update_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                  PRIMARY KEY (`id`) USING BTREE,
                                  UNIQUE INDEX `u_name`(`data_label`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_list
-- ----------------------------
INSERT INTO `sys_dict_list` VALUES (1, 'user_sex', '男', '1', 0, 1, 0, '男1', 1670468798, 1, 'admin', 1670551711, 1, 'admin');
INSERT INTO `sys_dict_list` VALUES (2, 'user_sex', '女', '0', 0, 1, 0, '女', 1670551703, 1, 'admin', 1670552921, 1, 'admin');
INSERT INTO `sys_dict_list` VALUES (3, 'auth_type', '目录', '0', 0, 1, 0, '目录', 1670575610, 1, 'admin', 0, 0, '');
INSERT INTO `sys_dict_list` VALUES (4, 'auth_type', '菜单', '1', 0, 1, 0, '菜单', 1670575619, 1, 'admin', 1670575636, 1, 'admin');
INSERT INTO `sys_dict_list` VALUES (5, 'auth_type', '按钮', '2', 0, 1, 0, '按钮', 1670575632, 1, 'admin', 0, 0, '');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
                            `id` int(11) NOT NULL AUTO_INCREMENT,
                            `ipaddr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                            `host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                            `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                            `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                            `param` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                            `result` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                            `start_time` int(11) NULL DEFAULT NULL,
                            `end_time` int(11) NULL DEFAULT NULL,
                            `user_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                            `spend_time` int(11) NULL DEFAULT NULL,
                            `type` int(11) NULL DEFAULT NULL,
                            `status` int(11) NULL DEFAULT NULL,
                            `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                            `del_flag` tinyint(1) NULL DEFAULT 0,
                            `create_time` int(11) NULL DEFAULT NULL,
                            `create_by` int(11) NULL DEFAULT NULL,
                            `create_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                            `update_time` int(11) NULL DEFAULT NULL,
                            `update_by` int(11) NULL DEFAULT NULL,
                            `update_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
                             `id` int(11) NOT NULL AUTO_INCREMENT,
                             `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `enable` tinyint(1) NULL DEFAULT NULL,
                             `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `del_flag` tinyint(1) NULL DEFAULT 0,
                             `create_time` int(11) NULL DEFAULT NULL,
                             `create_by` int(11) NULL DEFAULT NULL,
                             `create_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `update_time` int(11) NULL DEFAULT NULL,
                             `update_by` int(11) NULL DEFAULT NULL,
                             `update_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 'test', 'test', 1, 'xxx', 0, NULL, NULL, NULL, 1670575134, 1, 'admin');
INSERT INTO `sys_role` VALUES (2, 'admin', 'admin', 1, 'aaa', 0, 1670574008, 1, 'admin', 1670575100, 1, 'admin');

-- ----------------------------
-- Table structure for sys_role_auth
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_auth`;
CREATE TABLE `sys_role_auth`  (
                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `role_id` int(11) NULL DEFAULT NULL,
                                  `auth_id` int(11) NULL DEFAULT NULL,
                                  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                  `del_flag` tinyint(1) NULL DEFAULT 0,
                                  `create_time` int(11) NULL DEFAULT NULL,
                                  `create_by` int(11) NULL DEFAULT NULL,
                                  `create_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                  `update_time` int(11) NULL DEFAULT NULL,
                                  `update_by` int(11) NULL DEFAULT NULL,
                                  `update_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                  PRIMARY KEY (`id`) USING BTREE,
                                  UNIQUE INDEX `u_role_id_auth_id`(`role_id`, `auth_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 226 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_auth
-- ----------------------------
INSERT INTO `sys_role_auth` VALUES (1, 1, 1, '', 0, 1670747636, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (2, 1, 16, '', 0, 1670747636, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (3, 1, 17, '', 0, 1670747636, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (4, 1, 18, '', 0, 1670747636, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (5, 1, 19, '', 0, 1670747636, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (224, 2, 69, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (223, 2, 68, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (222, 2, 67, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (221, 2, 15, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (220, 2, 66, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (219, 2, 65, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (218, 2, 14, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (217, 2, 71, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (216, 2, 62, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (215, 2, 61, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (214, 2, 60, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (213, 2, 59, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (212, 2, 58, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (211, 2, 57, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (210, 2, 2, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (209, 2, 20, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (208, 2, 19, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (207, 2, 18, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (206, 2, 17, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (205, 2, 16, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (204, 2, 1, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (203, 2, 51, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (202, 2, 50, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (201, 2, 49, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (200, 2, 48, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (199, 2, 47, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (198, 2, 46, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (197, 2, 45, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (196, 2, 44, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (195, 2, 30, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (194, 2, 43, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (193, 2, 42, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (192, 2, 29, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (191, 2, 41, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (190, 2, 40, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (189, 2, 39, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (188, 2, 38, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (187, 2, 37, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (186, 2, 28, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (185, 2, 36, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (184, 2, 35, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (183, 2, 34, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (182, 2, 33, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (181, 2, 32, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (180, 2, 31, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (179, 2, 27, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (178, 2, 26, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (177, 2, 25, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (176, 2, 24, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (175, 2, 23, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (174, 2, 22, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (173, 2, 21, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (172, 2, 10, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (171, 2, 53, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (170, 2, 52, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (169, 2, 11, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (168, 2, 56, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (167, 2, 55, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (166, 2, 54, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (165, 2, 12, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (164, 2, 64, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (163, 2, 63, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (162, 2, 13, '', 0, 1671954147, 1, 'admin', 0, 0, '');
INSERT INTO `sys_role_auth` VALUES (225, 2, 70, '', 0, 1671954147, 1, 'admin', 0, 0, '');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
                             `id` int(11) NOT NULL AUTO_INCREMENT,
                             `real_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `nick_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `enable` tinyint(1) NULL DEFAULT NULL,
                             `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `sex` int(11) NULL DEFAULT NULL,
                             `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `login` int(11) NULL DEFAULT NULL,
                             `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `del_flag` tinyint(1) NULL DEFAULT 0,
                             `create_time` int(11) NULL DEFAULT NULL,
                             `create_by` int(11) NULL DEFAULT NULL,
                             `create_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             `update_time` int(11) NULL DEFAULT NULL,
                             `update_by` int(11) NULL DEFAULT NULL,
                             `update_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                             PRIMARY KEY (`id`) USING BTREE,
                             UNIQUE INDEX `i_username`(`username`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'ff', 'ff', 'admin', '123456', NULL, '17783738583', NULL, 'www.qq1624.com@qq.com', NULL, 'www.qq1624.com@qq.com', 0, NULL, NULL, NULL, 1670568459, 1, 'admin');
INSERT INTO `sys_user` VALUES (2, 'zhangfugui', 'fugui', 'zhy', '123456', 1, '17783738583', 1, 'www.qq1624.com@qq.com', NULL, 'zhy', 0, 1670559506, 1, 'admin', 1670566891, 1, 'admin');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `user_id` int(11) NULL DEFAULT NULL,
                                  `role_id` int(11) NULL DEFAULT NULL,
                                  `type` int(11) NULL DEFAULT NULL,
                                  `status` int(11) NULL DEFAULT NULL,
                                  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                  `del_flag` tinyint(1) NULL DEFAULT 0,
                                  `create_time` int(11) NULL DEFAULT NULL,
                                  `create_by` int(11) NULL DEFAULT NULL,
                                  `create_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                  `update_time` int(11) NULL DEFAULT NULL,
                                  `update_by` int(11) NULL DEFAULT NULL,
                                  `update_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                  PRIMARY KEY (`id`) USING BTREE,
                                  UNIQUE INDEX `u_user_id_role_id`(`user_id`, `role_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 2, 1, 0, 0, '', 0, 1670808663, 1, 'admin', 0, 0, '');
INSERT INTO `sys_user_role` VALUES (7, 1, 2, 0, 0, '', 0, 1670814644, 1, 'admin', 0, 0, '');

-- ----------------------------
-- Table structure for table_template
-- ----------------------------
DROP TABLE IF EXISTS `table_template`;
CREATE TABLE `table_template`  (
                                   `id` int(11) NOT NULL AUTO_INCREMENT,
                                   `type` int(11) NULL DEFAULT NULL,
                                   `status` int(11) NULL DEFAULT NULL,
                                   `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                   `del_flag` tinyint(1) NULL DEFAULT 0,
                                   `create_time` int(11) NULL DEFAULT NULL,
                                   `create_by` int(11) NULL DEFAULT NULL,
                                   `create_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                   `update_time` int(11) NULL DEFAULT NULL,
                                   `update_by` int(11) NULL DEFAULT NULL,
                                   `update_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                   PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of table_template
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
